.PHONY: init-stack init-stack-windows build-dev-init build-dev install-dev start-dev start-dev-windows stop-dev build-prod-init build-prod install-prod start-prod stop-prod

init-stack:
	sudo apt install npm
	cd php && npm install && npm run build

init-stack-windows:
	cd php & npm install & npm run build

build-dev:
	docker compose --env-file ./.env.dev build

build-dev-init:
	docker compose --env-file ./.env.dev.init build

install-dev: build-dev-init
	docker compose --env-file ./.env.dev run --no-deps --rm php composer install

start-dev:
	docker compose --env-file ./.env.dev up -d && cd php

start-dev-windows:
	docker compose --env-file ./.env.dev up -d & cd php

stop-dev:
	docker compose --env-file ./.env.dev down

build-prod-init:
	docker compose --env-file ./.env.init -f docker-compose.yml -f docker-compose.prod.yml build

build-prod:
	docker compose --env-file ./.env -f docker-compose.yml -f docker-compose.prod.yml build

install-prod: build-prod-init
	docker compose --env-file ./.env -f docker-compose.yml -f docker-compose.prod.yml run --no-deps --rm php composer install

start-prod:
	docker compose --env-file ./.env -f docker-compose.yml -f docker-compose.prod.yml up -d

stop-prod:
	docker compose --env-file ./.env -f docker-compose.yml -f docker-compose.prod.yml down
