# Project CI

## Getting Started

Afin de pouvoir tester en local, les étapes suivantes sont donc nécessaires.

### Prerequisites

Postulat de système d'exploitation : Ubuntu (22.04 LTS)

- docker desktop installé (https://www.docker.com/products/docker-desktop/) ou docker & docker compose (v2)
- (windows) avoir installé npm sur son ordi (https://radixweb.com/blog/installing-npm-and-nodejs-on-windows-and-mac)
- (Linux) git (sudo apt update && sudo apt install git)
- (Windows) git (https://git-scm.com/download/win)
- un éditeur de code (VS Code ou autre)
- être connecté au dépôt Git (échange clé ssh par exemple) :
  - sur le poste local, effectuer la commande suivante : ssh-keygen
  - lorsque la commande demande où sera stocké la clé, cliquer sur Entrée ou définir le chemin
  - définir le passphrase. La clé sera générée
  - Accéder au fichier .pub et copier le contenu sur le compte gitlab (Edition du profil, SSH Keys, ajouter une clé)
- taper dans une invite de commande : make -v
  - si la commande n'est pas reconnue suivez ces étapes sinon passer directement à l'Installing
  - télécharger MinGW https://sourceforge.net/projects/mingw/files/Installer/mingw-get/
  - lancer l'installation, décocher la case "...also install support..."
  - copier "C:\MinGW\bin" dans le PATH des variables d'environnement
  - taper en commande : mingw-get. Si la commande n'est pas reconnue redemarrer votre PC et réessayer
  - taper en commande : mingw-get install mingw32-make
  - aller dans le répertoire : C:\MinGW\bin et renommer le fichier mingw32-make.exe en make.exe
  - taper make -v, la commande devrait fonctionner



### Installing

Pour la première installation, il suffit d'effectuer les étapes suivantes :

- accéder au répertoire où le code sera généré
- exécuter la commande git clone `git@gitlab.com:DaniSoudry99/projectci.git`
- accéder au répertoire projectci
- exécuter les commandes suivantes :
  - (Windows) Vérifier que docker est bien lancé, si ce n'est pas le cas le demarrer
  - make init-stack (si on est sur linux) ou alors make init-stack-windows (si on est sur linux) ceci installera npm et fera le build des assets
  - make install-dev (Build des images avec le fichier d'env de dev + installation des dépendences du projet)
  - make start-dev (démarrage des containers)
  - sh command.sh (sur linux) ou ./command.sh (sur windows) -> (connexion au container php et initialisation de la base de données MySQL via commandes doctrine)

### Updates

Lorsque le code a été mis à jour (dichiers docker, fichiers docker-compose, ), effectuer les actions suivantes :

- make stop-dev (arrête les containers s'ils étaient exécutés)
- make build-dev (effectue un build des sevices avec en paramètre le fichier .env de dev) ou make build-prod pour l'environnement de production
- make start-dev (démarrage des containers)

Le code PHP étant monté comme un volume, aucune action n'est donc nécessaire.

Créez vous votre propre branche
- git checkout -b prenom.nom
