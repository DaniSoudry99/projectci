#!/bin/bash

set -e
docker exec -i skeleton-docker.php \sh -c "php bin/console cache:clear --env=prod; php bin/console doctrine:database:drop --if-exists -f --env=prod; php bin/console doctrine:database:create --env=prod; php bin/console doctrine:schema:update --force --env=prod"

resultat=$?
if [ $resultat -eq 0 ]; then 
	echo  "commande sucess"
else
	echo  "failed"
fi

