#!/bin/sh

set -e

if [[ ! -e ./public/build/manifest.json ]]; then
    mkdir -p ./public/build
    echo "{}" > ./public/build/manifest.json
fi

