<?php


namespace App\Tests\Unit;

use App\Entity\Voiture;
use App\Repository\VoitureRepository;
use App\Tests\UnitTester;

use function PHPUnit\Framework\assertEquals;

class VoitureTest extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    protected function _before()
    {
    }

    // tests
    public function testRegisterOnDatabase()
    {

        $voiture = \Codeception\Stub::make(
            new Voiture,
            ['getModele' => '3008', 'getMarque' => 'Peugeot', 'getCouleur' => 'Noir', 'getAnnee' => 2020]
        );
        $modele = $voiture->getModele();
        $marque = $voiture->getMarque();
        $couleur = $voiture->getCouleur();
        $annee = $voiture->getAnnee();

        $voitureRepository = $this->make(VoitureRepository::class, ['find' => $voiture]);

        $selectedVoiture = $voitureRepository->find($voiture);

        $this->assertEquals($marque, $selectedVoiture->getMarque());
        $this->assertEquals($modele, $selectedVoiture->getModele());
        $this->assertEquals($couleur, $selectedVoiture->getCouleur());
        $this->assertEquals($annee, $selectedVoiture->getAnnee());
    }
}
