<?php

namespace App\Form;

use App\Entity\Voiture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VoitureType
 * @package App\Form
 */
class VoitureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marque', TextType::class, [
                'label' => 'Nom de la marque'
            ])
            ->add('modele', TextType::class, [
                'label' => 'Nom du modèle'
            ])
            ->add('couleur', TextType::class, [
                'label' => 'Couleur'
            ])
            ->add('annee', IntegerType::class, [
                'label' => 'Année de création'
            ])
            ->add('photo', FileType::class, [
                'label' => "Photo de la voiture",
                'data_class' => null,
                'empty_data' => 'true',
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Voiture::class,
        ]);
    }
}
