<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Form\VoitureType;
use App\Repository\VoitureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VoitureController
 * @package App\Controller
 */
class VoitureController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * @param VoitureRepository $voitureRepository
     * @return Response
     *
     *
     * @Route("/", name="voiture_index", methods={"GET"})
     */
    public function index(VoitureRepository $voitureRepository): Response
    {
        return $this->render('voiture/index.html.twig', [
            'voitures' => $voitureRepository->findAll(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/voiture/new", name="voiture_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $voiture = new Voiture();
        $form = $this->createForm(VoitureType::class, $voiture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $file */
            $file = $form->get('photo')->getData();
            if ($file != null) {
                $extensionFile = $file->guessExtension();
                $fileName = $this->generateUniqueFileName() . '.' . $extensionFile;

                try {
                    $file->move(
                        $this->getParameter('voiture_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    dd($e->getMessage());
                }

                $voiture->setPhoto($fileName);
            }

            $this->entityManager->persist($voiture);
            $this->entityManager->flush();

            return $this->redirectToRoute('voiture_index', [
                'id' => $voiture->getId(),
            ]);
        }

        return $this->render('voiture/new.html.twig', [
            'voiture' => $voiture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Voiture $voiture
     * @return Response
     *
     * @Route("/voiture/{id}", name="voiture_show", methods={"GET"})
     */
    public function show(Voiture $voiture): Response
    {
        return $this->render('voiture/show.html.twig', [
            'voiture' => $voiture
        ]);
    }

    /**
     * @param Request $request
     * @param Voiture $voiture
     * @return Response
     * @Route("/voiture/edit/{id}", name="voiture_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Voiture $voiture): Response
    {
        $oldPhoto = $voiture->getPhoto();
        $form = $this->createForm(VoitureType::class, $voiture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('photo')->getData() == "true") {
                $voiture->setPhoto($oldPhoto);
            } else {
                /** @var UploadedFile $file */
                $file = $form->get('photo')->getData();

                $extensionFile = $file->guessExtension();
                $fileName = $this->generateUniqueFileName() . '.' . $extensionFile;

                try {
                    $file->move(
                        $this->getParameter('voiture_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                    dd($e->getMessage());
                }

                if (!empty($oldPhoto)) {
                    unlink($this->getParameter('voiture_directory') . "/" . $oldPhoto);
                }

                $voiture->setPhoto($fileName);
            }

            $this->entityManager->flush();

            return $this->render('voiture/show.html.twig', [
                'voiture' => $voiture
            ]);
        }

        return $this->render('voiture/edit.html.twig', [
            'voiture' => $voiture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName(): string
    {
        return md5(uniqid());
    }
}
