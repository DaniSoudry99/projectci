<?php

namespace App\DataFixtures;

use App\Entity\Voiture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {


        $data = [
            ["Peugeot", "208", "Noir", "2020", "peugeot-208.jpg"],
            ["Renault", "Megane", "Blanc", "2018", "renault-megane.jpg"],
            ["Mercedes", "Classe A", "Blanc", "2022", "mercedes-classea.jpg"],
            ["BMW", "M5", "Noir", "2015", "bmw-m5.jpg"],
            ["Audi", "Q3", "Bleu marine", "2020", "audi-q3.png"],
            ["Porsche", "911", "Noir", "2016", "porsche-911.jpg"],
        ];

        foreach ($data as $key => $value) {
            $voiture = new Voiture();
            $voiture->setMarque($value[0]);
            $voiture->setModele($value[1]);
            $voiture->setCouleur($value[2]);
            $voiture->setAnnee($value[3]);
            $voiture->setPhoto($value[4]);

            $filesystem = new Filesystem();
            $current_dir_path = getcwd();

            $filesystem->copy(
                $current_dir_path . '/public/voitureFixtures/' . $value[4],
                $current_dir_path . '/public/voiture/' . $value[4]
            );

            $manager->persist($voiture);
        }


        $manager->flush();
    }
}
